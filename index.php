<?php

require ('animal.php');
require ('frog.php');
require ('ape.php');

// sheep
$sheep = new Animal("shaun");

echo "Name : ". $sheep->name. "<br>"; // "shaun"
echo "Legs : ". $sheep->legs. "<br>"; // 4
echo "Cold Blooded : ". $sheep->cold_blooded. "<br>"; // "no"
echo "<br>";
// sheep end

// kodok
$kodok = new Frog("buduk");

echo "Name : ". $kodok->name. "<br>";
echo "Legs : ". $kodok->legs. "<br>";
echo "Cold blooded : ". $kodok->cold_blooded. "<br>"; 
$kodok->jump() ; // "hop hop";
echo "<br><br>";
// kodok end

// sungokong
$sungokong = new Ape("kera sakti");

echo "Name : ". $sungokong->name. "<br>";
echo "Legs : ". $sungokong->legs. "<br>";
echo "Cold blooded : ". $sungokong->cold_blooded. "<br>"; 
$sungokong->yell() // "Auooo";
// sungokong end

?>