@extends('layout.master')

@section('judul')
    Halaman Detail Cast
@endsection

@section('content')
<a href="/cast" class="btn btn-success btn-sm">Kembali</a>
<br><br>
<h1>{{$data->nama}}</h1>
<p>{{$data->bio}}</p>
@endsection