@extends('layout.master')

@section('judul')
    Halaman Ubah Cast
@endsection

@section('content')
<form action="/cast/{{$data->id}}" method="POST">
    @csrf
    @method("PUT")
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="mb-3">
        <label>Nama</label>
      <input type="text" value="{{$data->nama}}" name="nama" class="form-control">
    </div>

    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="mb-3">
      <label>Umur</label>
      <input type="text" value="{{$data->umur}}" class="form-control" name="umur">
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    <div class="mb-3">
      <label>Bio</label>
      <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{$data->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
  <br>
  <a href="/cast"><button type="submit" class="btn btn-danger">Batal</button></a> 
@endsection