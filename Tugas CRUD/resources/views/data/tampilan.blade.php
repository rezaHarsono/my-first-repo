@extends('layout.master')

@section('judul')
    Halaman Tampilan Cast
@endsection


@section('content')
<a href="/cast/create" class="btn btn-warning btn-sm">Tambah</a>
<br><br>
<table class="table table-striped">
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">umur</th>
        <th scope="col">action</th>
      </tr>
      </thead>
      <tbody>
        @forelse ($cast as $key => $value)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$value->nama}}</td>
          <td>{{$value->umur}}</td>
          <td>
            <form action="/cast/{{$value->id}}" method="POST">
              <a href="/cast/{{$value->id}}" class="btn btn-success btn-sm">Detail</a>
              <a href="/cast/{{$value->id}}/edit" class="btn btn-primary btn-sm">Edit</a>
              @method('delete')
              @csrf
              <input type="submit" value="delete" class="btn btn-danger btn-sm"> 
          </td>
        </tr>
        @empty
          <h1>No Data</h1>
        @endforelse
    </tbody>
  </table>
@endsection