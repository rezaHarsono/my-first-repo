<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function index()
    {
        $cast = DB::table('cast')->get();
        // dd($cast);

        return view('data.tampilan', ['cast' => $cast]);

    }

    public function show($id)
    {
        $data = DB::table('cast')->find($id);
 
        return view('data.detail', ['data' => $data]);
    }

    public function create() 
    {
        return view('data.tambah');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [   'nama.required' => 'Nama Harus Diisi Minimal 5 Huruf!',
            'umur.required' => 'Umur Harus Diisi!',
            'bio.required' => 'Bio Tidak Boleh Kosong'

        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio']
        ]);

        return redirect('/cast');
    }

    public function edit($id)
    {
        $data = DB::table('cast')->find($id);
 
        return view('data.edit', ['data' => $data]);
    }
    
    public function update($id, Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required|min:5',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [   'nama.required' => 'Nama Harus Diisi Minimal 5 Huruf!',
            'umur.required' => 'Umur Harus Diisi!',
            'bio.required' => 'Bio Tidak Boleh Kosong'

        ]);

        DB::table('cast')
              ->where('id', 1)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio']
                ]
            );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id',"=", $id)->delete();

        return redirect('/cast');
    }
}

