<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', [HomeController::class, 'beranda']);
Route::get('/register', [AuthController::class, 'form']);
Route::post('/welcome', [AuthController::class, 'success']);

Route::get('/table', function ()
{
    return view('halaman.table');
});

Route::get('/data-table', function ()
{
    return view('halaman.data-table');
});

// CRUD
// Create
// Form tambah cast
Route::get('/cast/create', [CastController::class, 'create']);
// kirim data cast ke database
Route::post('cast', [CastController::class, 'store']);
// Read
// Menampilkan Data cast
Route::get('/cast', [CastController::class, 'index']);
// Detail Cast
Route::get('/cast/{cast_id}', [CastController::class, 'show']);
// Form untuk edit data cast
Route::get('/cast/{cast_id}/edit',[CastController::class, 'edit']);
// Edit data
Route::put('/cast/{cast_id}', [CastController::class, 'update']);
// Delete
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']);