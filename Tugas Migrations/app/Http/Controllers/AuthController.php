<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() 
    {
        return view('halaman.pendaftaran');
    }

    public function success(Request $request) 
    {
        $firstName = $request['first'];
        $lastName = $request['last'];

        return view('halaman.selesai', ['firstName' => $firstName, 'lastName' => $lastName]);
    }
};
